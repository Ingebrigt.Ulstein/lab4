package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    int cols;
    int rows;
    CellState[][] cells;

    public CellGrid(int rows, int columns, CellState initialState) {
		this.cols = columns;
        this.rows = rows;
        this.cells = new CellState[rows][columns];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                cells[i][j] = initialState;
            }
        }
	}

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        try {
            this.cells[row][column] = element;
        }
        catch (IllegalArgumentException e) {
            throw e;
        }
    }

    @Override
    public CellState get(int row, int column) {
        try {
           return this.cells[row][column];
        }
        catch (IllegalArgumentException e) {
            throw e;
        }
    }

    @Override
    public IGrid copy() {
        IGrid CelleGrid = new CellGrid(numRows(), numColumns(), CellState.ALIVE);
        for (int i = 0; i < numRows(); i++) {
            for (int j = 0; j < numColumns(); j++) {
                CelleGrid.set(i,j,this.get(i,j));
            }
        }
        return CelleGrid;
    }
    
}
